const gulp = require("gulp");
const browserSync = require("browser-sync");
var nodemon = require("gulp-nodemon");

gulp.task("nodemon", cb => {
    let started = false;
    return nodemon({
        script: './bin/www', //Ejecucion del servidor
        ext: 'js twig json css', //Archivos a los cuales mirar por cambios
        ignore: ['gulpfile.js', 'node_modules/']
    }).on("start", () => {
        if (!started) {
            cb();
            started = true;
        }
    }).on('restart', () => {
        setTimeout(() => {
            browserSync.reload();
        }, 2000);
    });
});

gulp.task(
    "browser-sync",
    gulp.series("nodemon", () => {
        browserSync.init(null, {
            proxy: "localhost:80",
            files: ["public/**/*.*"],
            notify: true,
            port: 9000,
            open: false
        });
    })
);

gulp.task("default", gulp.series("browser-sync", () => {}));
