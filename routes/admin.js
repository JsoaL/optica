var express = require('express');
var router = express.Router();
var admin = require('../controllers/AdminController.js');

/*router.get('/create', admin.create);
router.post('/save', admin.save);*/
router.post('/login', admin.login);

module.exports = router;