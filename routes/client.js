var express = require('express');
var router = express.Router();
var client = require('../controllers/ClientController.js');

//router.get('/save', client.save);
router.post('/add', client.add);
//router.post('/load', client.load);
router.post('/listAll', client.listAll);
router.post('/details', client.details);
router.post('/getConsultById', client.getConsultById);
router.post('/getNotaById', client.getNotaById);
router.post('/doSearch', client.doSearch);
router.post('/oneById',client.oneById);
router.post('/addTest',client.addTest);
router.post('/editUser',client.editUser);
router.post('/deleteUser',client.deleteUser);
router.post('/updateTest',client.updateTest);
router.post('/dropTest', client.dropTest);

module.exports = router;