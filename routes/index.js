var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  if(!req.session.user)
    res.render('clients/index',{salir: true, active:'active'});
  else
    res.render('clients/index',{carrusel: true,opcion:`Bienvenido: "${req.session.user.toString().toUpperCase()}"`});
});

router.get('/agregar',(req,res,nex)=>{
  if(req.session.user)
    res.render('clients/add',{add:'active show',opcion:'Nuevo Cliente'});
  else
    res.redirect('/');
});

router.get('/buscar', (req, res, nex) => {
  if(req.session.user)
    res.render('clients/search',{ pointer:'historial',opcion:'Busqueda' });
  else
    res.redirect('/');
});

router.get('/listar', (req, res, nex) => {
  if(req.session.user)
   res.render('clients/list',{ pointer:'historial',opcion:'Listado' });
  else
    res.redirect('/');
});

router.get('/importar', (req, res, nex) => {
  if(req.session.user)
    res.render('clients/import');
  else
    res.redirect('/');
});

router.post('/historial', (req, res, nex) => {
  if(req.session.user)
    res.render('clients/historial',{ id: req.body.id, pointer:'examen',opcion:'Historial' });
  else
    res.redirect('/');
});

router.post('/examen', (req, res, nex) => {
  if(req.session.user)
    res.render('clients/test',{ id: req.body.id, disabled:'disabled', test:'active show',opcion:'Nueva Consulta'});
  else
    res.redirect('/');
});

router.post('/editar', (req, res, nex) => {
  if(req.session.user)
    res.render('clients/edit',{ id: req.body.id, add:'active show',opcion:'Editar'});
  else
    res.redirect('/');
});

module.exports = router;
