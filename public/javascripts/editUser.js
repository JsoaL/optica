$(() => {
    //Validaciones con pattern
    $('#nombre, #paterno, #materno, #poblacion').attr({
        'pattern': "^[a-zA-ZÀ-ÿ\\u00f1\\u00d1]+(\\s*[a-zA-ZÀ-ÿ\\u00f1\\u00d1]*)*[a-zA-ZÀ-ÿ\\u00f1\\u00d1]+$",
        'title': "Por favor, introduce solo letras."
    });
    const hoy = new Date();
    const mes = hoy.getMonth() + 1;
    $('#fecha').val(`${hoy.getFullYear()}-${(mes>=10)?`${mes}`:`0${mes}`}-${hoy.getDate()}`);
    const inputs = document.querySelectorAll('input, textarea');
    inputs.forEach((input) => {
        input.addEventListener('invalid', () => {
            input.classList.add('invalid');
        })
        input.addEventListener('input', () => {
            if (input.validity.valid)
                input.classList.remove('invalid');
        })
    })
    const url = "client/oneById";
    if(user){
        asyncPostReq({id:user}, url, 'application/json')
        .then( data =>{
            if (data) {
                for (const key in data) {
                    if(key === 'nacimiento'){
                        const date = data[key].split('T')[0].split('-');
                        $(`#${key}`).val(`${date[0]}-${date[1]}-${date[2]}`);
                    } else if(key === 'usa')
                        (data[key]==='SI')?$(`#${key}Si`).prop('checked', true):$(`#${key}No`).prop('checked', true);
                    else {
                        $(`label[for='${key}']`).addClass('active');
                        $(`#${key}`).val(data[key]);
                    }
                }
            }
        }).catch(e => console.error(e));
    }else{
        $('#btnEditUser').attr("disabled", true);
    }
});

$('#btnEditUser').click((e) => {
    e.preventDefault();
    let params = {
        id: user,
        nombre: $('#nombre').val(),
        paterno: $('#paterno').val(),
        materno: $('#materno').val(),
        edad: $('#edad').val(),
        trabajo: $('#trabajo').val(),
        usa: $('input:radio[name=usaLentes]:checked').val(),//($('.state, .p-on').css('display') != 'none') ? "SI" : "NO", //Usa lentes
        telefono: $('#telefono').val(),
        direccion: $('#direccion').val(),
        poblacion: $('#poblacion').val()
    };
    for (const key in params)
        if (typeof params[key] === 'string') params[key] = params[key].trim().toUpperCase();
    if(!params.nacimiento) delete params.nacimiento
    if (params) {
        console.log(params);
        let url = "client/editUser";
        asyncPostReq(params, url, 'application/json')
        .then( data => {
            if (data) {
                if (data.status === "F")
                    createNotify('pe-7s-close-circle', `Error, <b>INTENTE de nuevo</b>.`, 'danger', 1000, 5000);
                 else {
                    $('#btnEditUser').attr("disabled", true);
                    createNotify('pe-7s-check', `<b>EXITO</b>, Datos actualizados correctamente.`, 'success', 1000, 5000);
                    setTimeout(() => { window.location.href = "/"; }, 3000);
                }
            }
        }).catch(e => console.error(e));
    } else {
        createNotify('pe-7s-attention', `<b>Complete</b> el formulario para continuar.`, 'warning', 1000, 5000);
    }
    $('#basics-tab').tab('show');
});