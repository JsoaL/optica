const resetFormtwo = () => {
    $('#fecha').val('');
    $('#examino').val('');
    $('#esfIzq').val('');
    $('#esfDer').val('');
    $('#cilIzq').val('');
    $('#cilDer').val('');
    $('#ejeIzq').val('');
    $('#ejeDer').val('');
    $('#addIzq').val('');
    $('#addDer').val('');
    $('#nota').val('');
    $('#lente').val('');
    $('#color').val('');
    $('#armazon').val('');
    $('#oblea').val('');
    $('#di').val('');
    $('#observaciones').val('');
};

$(() => {
    //Validaciones con pattern
    $('#nombre, #paterno, #materno, #poblacion').attr({
        'pattern': "^[a-zA-ZÀ-ÿ\\u00f1\\u00d1]+(\\s*[a-zA-ZÀ-ÿ\\u00f1\\u00d1]*)*[a-zA-ZÀ-ÿ\\u00f1\\u00d1]+$",
        'title': "Por favor, introduce solo letras."
    });
    const hoy = new Date();
    const mes = hoy.getMonth() + 1;
    $('#fecha').val(`${hoy.getFullYear()}-${(mes>=10)?`${mes}`:`0${mes}`}-${hoy.getDate()}`);
    const inputs = document.querySelectorAll('input, textarea');
    inputs.forEach((input) => {
        input.addEventListener('invalid', () => {
            input.classList.add('invalid');
        })
        input.addEventListener('input', () => {
            if (input.validity.valid)
                input.classList.remove('invalid');
        })
    })
    const url = "client/oneById";
    asyncPostReq({id:user}, url, 'application/json')
    .then( data =>{
        if (data) {
            for (const key in data) {
                if(key === 'nacimiento'){
                    const date = data[key].split('T')[0].split('-');
                    $(`#${key}`).val(`${date[0]}-${date[1]}-${date[2]}`);
                } else if(key === 'usa')
                    (data[key]==='SI')?$(`#${key}Si`).prop('checked', true):$(`#${key}No`).prop('checked', true);
                else {
                    $(`label[for='${key}']`).addClass('active');
                    $(`#${key}`).val(data[key]);
                }
            }
        }
    }).catch(e => console.error(e));
});

$('#btnReset').click(()=>{
    resetFormtwo();
});

$('#btnAddConsul').click((e) => {
    e.preventDefault();
    let params = {
        id: user,
        consulta: {
            fecha: $('#fecha').val(),
            examino: $('#examino').val(),
            esf: {
                izq: $('#esfIzq').val(),
                der: $('#esfDer').val()
            },
            cil: {
                izq: $('#cilIzq').val(),
                der: $('#cilDer').val()
            },
            eje: {
                izq: $('#ejeIzq').val(),
                der: $('#ejeDer').val()
            },
            add: {
                izq: $('#addIzq').val(),
                der: $('#addDer').val()
            },
            lentes: {
                nota: $('#nota').val(),
                lente: $('#lente').val(),
                color: $('#color').val(),
                armazon: $('#armazon').val(),
                oblea: $('#oblea').val(),
                di: $('#di').val(),
                observaciones: $('#observaciones').val()
            }
        }
    };
    trimUp(params.consulta);
    trimUp(params.consulta.lentes);
    if (params) {
        let url = "client/addTest";
        asyncPostReq(params, url, 'application/json')
        .then( data =>{
            if (data) {
                resetFormtwo();
                if (data.status === "F")
                    createNotify('pe-7s-close-circle', `Error, <b>INTENTE de nuevo</b>.`, 'danger', 1000, 5000);
                 else {
                     $('#btnAddConsul').attr("disabled", true);
                     $('#btnReset').attr("disabled", true);
                    createNotify('pe-7s-check', `<b>EXITO</b>, Examen creado correctamente.`, 'success', 1000, 5000);
                    setTimeout(() => {
                        $('#formModalConsultas').attr('action', '/historial');
                        $('#idUser').val(user);
                        $('#btnSubmit').trigger('click');
                    }, 1500);
                }
            }
        }).catch(e => console.error(e));
    } else {
        createNotify('pe-7s-attention', `<b>Complete</b> el formulario para continuar.`, 'warning', 1000, 5000);
    }
    $('#basics-tab').tab('show');
});