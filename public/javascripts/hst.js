$('#tableDetails').bootstrapTable({ showHeader: false });//btnAddConsul

$('#tableConsultas').on('dbl-click-cell.bs.table', (field, value, row, element) => {
    if(value !== 'detalles') loadConsultaNota(element._id);
});

const loadConsultaNota = (id) => {
    if(id){
        $(".btnSaveChanges").attr("hidden", true);
        $("input[class$='form-control']").prop('disabled', true);
        $("textarea[class$='form-control']").prop('readonly', true);
        $("textarea[class$='form-control']").val('');
        $('#formDetails').trigger("reset");
        asyncPostReq({ idConsulta: id }, 'client/getConsultById', 'application/json')
            .then(data => {
                if (data) {
                    const consulta = data.consultas[0];
                    for (const key in consulta) {
                        const tipo = consulta[key];
                        if (key !== "_id" && key !== "lentes") {
                            for (const key2 in tipo) {
                                const keyToUpper = key2.charAt(0).toUpperCase() + key2.slice(1);
                                $(`#${key}${keyToUpper}`).text(tipo[key2]);
                            }
                        }
                    }
                    asyncPostReq({ idNota: id }, 'client/getNotaById', 'application/json')
                    .then(data => {
                        if (data) {
                            for (const key in data) {
                                if (key !== "_id") {
                                    if (key === "observaciones")
                                        $(`#${key}`).val(data[key]);
                                    else
                                        $(`#${key}`).val(data[key]);
                                }
                            }
                            $('#fullHeightModalBottom').modal({ //Desplegar modal
                                backdrop: false,
                                keyboard: true,
                                show: true
                            });
                        }
                    }).catch(e => console.error(e));
                }
            }).catch(e => console.error(e));
    } else createNotify('pe-7s-attention', `<b>Advertencia</b> Por favor, intente de nuevo.`, 'warning', 1000, 5000);
};

const editTest = (id) => {
    if(id){
        $(".btnSaveChanges").attr("hidden", false);
        $("input[class$='form-control']").prop('disabled', false);
        $("textarea[class$='form-control']").prop('readonly', false);
        $("textarea[class$='form-control']").val('');
        $('#formDetails').trigger("reset");
        asyncPostReq({ idConsulta: id }, 'client/getConsultById', 'application/json')
            .then(data => {
                if (data) {
                    const consulta = data.consultas[0];
                    for (const key in consulta) {
                        const tipo = consulta[key];
                        if (key !== "_id" && key !== "lentes") {
                            for (const key2 in tipo) {
                                const keyToUpper = key2.charAt(0).toUpperCase() + key2.slice(1);
                                $(`#${key}${keyToUpper}`).text(tipo[key2]);
                            }
                        }
                    }
                    asyncPostReq({ idNota: id }, 'client/getNotaById', 'application/json')
                    .then(data => {
                        if (data) {
                            for (const key in data) {
                                if (key !== "_id") {
                                    if (key === "observaciones")
                                        $(`#${key}`).val(data[key]);
                                    else
                                        $(`#${key}`).val(data[key]);
                                }
                            }
                            $('#fullHeightModalBottom').modal({ //Desplegar modal
                                backdrop: false,
                                keyboard: true,
                                show: true
                            });
                            $('.btnSaveChanges').attr("id", id);
                        }
                    }).catch(e => console.error(e));
                }
            }).catch(e => console.error(e));
    } else createNotify('pe-7s-attention', `<b>Advertencia</b> Por favor, intente de nuevo.`, 'warning', 1000, 5000);
};

const deleteTest = (id) => {
    if(id){
        $('#idTest').val(id);
        $('#modalRemove').modal({ //Desplegar modal
            backdrop: 'static',
            keyboard: false,
            show: true
        });
        $('#btnContinue').click((e)=>{
            $('#btnContinue').attr("disabled", true);
            e.preventDefault();
            asyncPostReq({id: id}, 'client/dropTest', 'application/json').then(data => {
                if (data) {
                    if (data.status === "S") {
                        $('#modalRemove').modal('hide');
                        createNotify('pe-7s-check', `<b>EXITO</b>, Consulta eliminada correctamente.`, 'success', 1000, 5000);
                        setTimeout(() => {
                            $('#formModalConsultas').attr('action', '/historial');
                            $('#idUser').val(user);
                            $('#btnSubmit').trigger('click');
                        }, 1500);
                    } else
                        createNotify('pe-7s-close-circle', `Error al eliminar, <b>INTENTE de nuevo</b>.`, 'danger', 1000, 5000);
                }
            }).catch(e => console.error(e));
        });
    } else createNotify('pe-7s-attention', `<b>Advertencia</b> Por favor, intente de nuevo.`, 'warning', 1000, 5000);
};

$('#btnAddConsul').click(()=>{
    $('#modalConsultas').modal({ //Desplegar modal
        backdrop: 'static',
        keyboard: false,
        show: true
    });
    $('#infop').empty();
    $('#infop').append(`¿Nueva consulta para <strong>${$('#nombreCompleto').text()}</strong>?`);
    $('#idUser').val(user);
});

$('.btnSaveChanges').click((e)=>{
    let params = {
        id: e.target.id,
        consulta: {
            fecha: $('#fecha').val(),
            examino: $('#examino').val(),
            esf: {
                izq: $('#esfIzq').val(),
                der: $('#esfDer').val()
            },
            cil: {
                izq: $('#cilIzq').val(),
                der: $('#cilDer').val()
            },
            eje: {
                izq: $('#ejeIzq').val(),
                der: $('#ejeDer').val()
            },
            add: {
                izq: $('#addIzq').val(),
                der: $('#addDer').val()
            },
            lentes: [{
                nota: $('#nota').val(),
                lente: $('#lente').val(),
                color: $('#color').val(),
                armazon: $('#armazon').val(),
                oblea: $('#oblea').val(),
                di: $('#di').val(),
                observaciones: $('#observaciones').val()
            }]
        }
    };
    trimUp(params.consulta);
    trimUp(params.consulta.lentes[0]);
    if (params) {
        let url = "client/updateTest";
        asyncPostReq(params, url, 'application/json')
        .then( data =>{
            if (data) {
                if (data.status === "F")
                    createNotify('pe-7s-close-circle', `Error, <b>INTENTE de nuevo</b>.`, 'danger', 1000, 5000);
                 else {
                    createNotify('pe-7s-check', `<b>EXITO</b>, Edición terminada correctamente.`, 'success', 1000, 5000);
                    $('#fullHeightModalBottom').modal('hide');
                    $(`#${e.target.id}`).attr("disabled", true);
                    setTimeout(() => {
                        $('#formModalConsultas').attr('action', '/historial');
                        $('#idUser').val(user);
                        $('#btnSubmit').trigger('click');
                    }, 1500);
                }
            }
        }).catch(e => console.error(e));
    } else createNotify('pe-7s-attention', `<b>Complete</b> el formulario para continuar.`, 'warning', 1000, 5000);
});

$(() => {
    const preInputs = [{dato:"Nota:",detalle:`<input style="text-transform: uppercase; color: #b50101;" id="nota" type="text" class="form-control" placeholder="Folio" value="" style="text-transform: uppercase;" disabled>`},
    {dato:"Alt. De Oblea:",detalle:`<input style="text-transform: uppercase; color: #3e3e3e;" id="oblea" type="text" class="form-control" placeholder="Oblea" value="" style="text-transform: uppercase;" disabled>`},
    {dato:"Lentes:",detalle:`<input style="text-transform: uppercase; color: #3e3e3e;" id="lente" type="text" class="form-control" placeholder="Lentes" value="" style="text-transform: uppercase;" disabled>`},
    {dato:"Color:",detalle:`<input style="text-transform: uppercase; color: #3e3e3e;" id="color" type="text" class="form-control" placeholder="Color" value="" style="text-transform: uppercase;" disabled>`},
    {dato:"Armazon:",detalle:`<input style="text-transform: uppercase; color: #3e3e3e;" id="armazon" type="text" class="form-control" placeholder="Armazon" value="" style="text-transform: uppercase;" disabled>`},
    {dato:"D.I.",detalle:`<input style="text-transform: uppercase; color: #3e3e3e;" id="di" type="text" class="form-control" placeholder="D.I." value="" style="text-transform: uppercase;" disabled>`}]
    $('#tableDetails').bootstrapTable('load', preInputs);
    asyncPostReq({user:user},'client/details','application/json')
        .then( res =>{
            if(res){
                let {nombre,paterno,materno,edad,registro,consultas} = res;
                const dReg = new Date(registro);
                $('#nombreCompleto').text(`${nombre} ${paterno} ${materno}`);
                $('#edad').text(`${(edad)?edad+" años":"-"}`);
                $('#reg').text(`${parseInt(dReg.getUTCDate())}/${parseInt(dReg.getMonth())+1}/${dReg.getFullYear()}`);
                for (const consulta of consultas) {
                    const d = new Date(consulta.fecha);
                    const n = consulta.lentes[0].nota;
                    consulta.date = `${parseInt(d.getUTCDate())}/${parseInt(d.getMonth())+1}/${d.getFullYear()}`;
                    consulta.detalles = `<button id="${consulta._id}" type="button" rel="tooltip" title="Mostrar detalles" class="btnDetails btn btn-success btn-simple btn-xs btn-fill waves-effect waves-light" data-original-title="Edit Task">
                                            <i id="${consulta._id}"  style="color: white;" class="fa fa-info"></i>
                                        </button>
                                        <button id="${consulta._id}" type="button" rel="tooltip" title="Editar" class="btn btn-info btn-simple btn-xs waves-effect waves-light eventEditar" data-original-title="Edit Task">
                                            <i id="${consulta._id}"  style="color: white;" class="fa fa-edit"></i>
                                        </button>
                                        <button id="${consulta._id}" type="button" rel="tooltip" title="Eliminar" class="btn btn-danger btn-simple btn-xs waves-effect waves-light eventEliminar" data-original-title="Remove">
                                            <i id="${consulta._id}"  style="color: white;" class="fa fa-times"></i>
                                        </button>
                                        `;
                    consulta.nota = `<span style="color: #b50101;">${(!n)?'-':n}</span>`;
                }
                $('#tableConsultas').bootstrapTable('load',consultas);
                $('.btnDetails').click((e) => { loadConsultaNota(e.target.id); });
                $('.eventEditar').click((e)=>{editTest(e.target.id)});
                $('.eventEliminar').click((e)=>{deleteTest(e.target.id)});
            }
        }).catch(e => console.error(e));
});
