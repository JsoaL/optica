//Variables
const tableId = '#tableTest';
//Methods
const eventOptionsRow = (tipo, event) => {
    const user = (event.currentTarget.parentElement.parentElement.innerText).split('\t')[1];
    const id = (event.currentTarget.parentElement.parentElement.attributes)[0].value;
    $('#modalConsultas').modal({ backdrop: 'static', keyboard: false, show: true });
    $('#infop').empty();
    if(tipo === '/editar') $('#infop').append(`¿Editar al usuario: <strong>${user}</strong>?`);
    else $('#infop').append(`¿Eliminar al usuario: <strong>${user}</strong>?<br>
    <strong style="color: crimson;">Advertencia</strong>
    <br>Todas sus consultas serán eliminadas.<br>
    <div class="md-form">
        <input type="text" id="inputConfirmar" class="form-control">
        <label for="inputConfirmar">Escriba "SI" para confirmar</label>
    </div>`);
    $('#formModalConsultas').attr('action', tipo);
    $('#idUser').val(id);
    $('#btnSubmit').off();
    if(tipo === '/eliminar'){
        $('#btnSubmit').click((e)=>{
            e.preventDefault();
            const msgConfirmacion = $('#inputConfirmar').val().trim();
            if(msgConfirmacion === 'SI'){
                asyncPostReq({id: id}, 'client/deleteUser', 'application/json').then(data => {
                    if (data) {
                        if (data.status === "S") {
                            $('#modalConsultas').modal('hide');
                            createNotify('pe-7s-check', `<b>EXITO</b>, Usuario eliminado correctamente.`, 'success', 1000, 5000);
                            $("#btnLoadAll").trigger( "click" );
                            $('#btnSubmit').off();
                        } else
                            createNotify('pe-7s-close-circle', `Error al eliminar, <b>INTENTE de nuevo</b>.`, 'danger', 1000, 5000);
                    }
                }).catch(e => console.error(e));
            } else
                createNotify('pe-7s-attention', `<b>Confirme</b> la eliminación del usuario.`, 'warning', 1000, 5000);
        });
    }
};
//Inicializar tabla de usuarios
$(tableId).bootstrapTable({
    buttonsPrefix: 'btn btn-sm',
    buttonsClass: 'dark',
    pagination: true,
    search: true,
    //searchOnEnterKey: true,
    pageSize: 7,
    pageList: [7],
    showSearchButton: true,
    showSearchClearButton: true,
    icons: {
        search: 'fa fa-search',
        clearSearch: 'fa fa-trash',
        detailOpen: 'fa fa-plus',
        detailClose: 'fa fa-minus'
    },
    detailView: true,
    detailViewIcon: true,
    detailViewByClick: false,
    detailFormatter: (index, row, element) => {
        const {
            telefono,
            direccion,
            poblacion,
            trabajo
        } = row;
        return `<div class="row">
                    <div class="col-md-8"><b>Teléfono(s):</b> ${telefono}</div>
                    <div class="col-md-4"><b>Ocupación:</b> ${trabajo}</div>
                </div>
                <div class="row">
                    <div class="col-md-8"><b>Dirección:</b> ${direccion} </div>
                    <div class="col-md-4"><b>Población:</b> ${poblacion}</div>
                </div>`;
    }
});
//Header de la tabla
$('.fixed-table-toolbar').prepend(`
    <div class="pull-left search input-group">
        <div class="input-group">
            <button class="btn btn-secondary waves-effect waves-light" name="cargar" id="btnLoadAll" type="button">
                Cargar
                <i class="fa fa-download" aria-hidden="true"></i>
            </button>
            <button class="btn btn-dark waves-effect waves-light" name="limpiar" id="bntCleanSearch" type="button">
                Limpiar Tabla
                <i class="fa fa-eraser" aria-hidden="true"></i>
            </button>
        </div>
    </div>`);

//Estilos de los botones de la tabla
$('.fixed-table-toolbar button').removeClass("btn-default");
$('.fixed-table-toolbar button').addClass("btn-info btn-fill");

//Add event to the button for the list view
$('#btnLoadAll').click(() => {
    asyncPostReq({data: true}, 'client/listAll', 'application/json') .then(data => {
        if (data) {
            for (let key in data) {
                const dReg = new Date(data[key].registro);
                const {
                    nombre,
                    paterno,
                    materno,
                    edad
                } = data[key];
                data[key].registro = `${parseInt(dReg.getUTCDate())}/${parseInt(dReg.getUTCMonth()+1)}/${dReg.getFullYear()}`;
                data[key].edad = `${(edad)?edad+" años":"-"}`;
                data[key].nombreCompleto = `${nombre} ${paterno} ${materno}`;
                data[key].actions = `<button type="button" rel="tooltip" title="Editar" class="btn btn-info btn-simple btn-xs waves-effect waves-light eventEditar" data-original-title="Edit Task">
                                        <i style="color: white;" class="fa fa-edit"></i>
                                     </button>
                                     <button type="button" rel="tooltip" title="Eliminar" class="btn btn-danger btn-simple btn-xs waves-effect waves-light eventEliminar" data-original-title="Remove">
                                        <i style="color: white;" class="fa fa-times"></i>
                                     </button>`;
            }
            $(tableId).bootstrapTable('load', data);
            $(`.eventEditar`).click((e)=>{ eventOptionsRow('/editar', e);});
            $(`.eventEliminar`).click((e)=>{ eventOptionsRow('/eliminar', e);});
        }
    }).catch(e => console.error(e));
});

$('#bntCleanSearch').click(() => {
    if ($(tableId).bootstrapTable('getData').length!=0) $(tableId).bootstrapTable('removeAll');
});

$(tableId).on('dbl-click-cell.bs.table', (field, value, row, element) => {
    if(value){
        $('#modalConsultas').modal({ //Desplegar modal
            backdrop: 'static',
            keyboard: false,
            show: true
        });
        $('#infop').empty();
        $('#infop').append(`¿Mostrar historial de: <strong>${element.nombreCompleto}</strong>?`);
        $('#formModalConsultas').attr('action', '/historial');
        $('#idUser').val(element._id);
    }
});

$(tableId).on('page-change.bs.table', (number, size) => {
    $(`.eventEditar`).click((e)=>{ eventOptionsRow('/editar', e);});
    $(`.eventEliminar`).click((e)=>{ eventOptionsRow('/eliminar', e);});
});

//Document ready
$(() => {$("#btnLoadAll").trigger( "click" );});