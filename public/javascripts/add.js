$(() => {
    //Validaciones con pattern
    const hoy = new Date();
    const mes = hoy.getMonth() + 1;
    $('#fecha').val(`${hoy.getFullYear()}-${(mes>=10)?`${mes}`:`0${mes}`}-${hoy.getDate()}`);
    const inputs = document.querySelectorAll('input, textarea');
});

$('#btnReset').click(()=>{
    resetForm();
});

$('#aceptar').click((e) => {
    e.preventDefault();
    const boolNam = $('#nombre')[0].checkValidity();
    const boolPat = $('#paterno')[0].checkValidity();
    const boolMat = $('#materno')[0].checkValidity();
    if(!boolNam | !boolPat | !boolMat){
        (!boolNam)?$('#nombre').val(''):null;
        (!boolPat)?$('#paterno').val(''):null;
        (!boolMat)?$('#materno').val(''):null;
        createNotify('pe-7s-attention', `<b>Ingrese</b> un nombre valido.`, 'warning', 1000, 5000);
    }
    else {
        let params = {
            nombre: $('#nombre').val(),
            paterno: $('#paterno').val(),
            materno: $('#materno').val(),
            edad: $('#edad').val(),
            trabajo: $('#trabajo').val(),
            usa: $('input:radio[name=usaLentes]:checked').val(),//($('.state, .p-on').css('display') != 'none') ? "SI" : "NO", //Usa lentes
            telefono: $('#telefono').val(),
            direccion: $('#direccion').val(),
            poblacion: $('#poblacion').val(),
            consulta: {
                fecha: $('#fecha').val(),
                examino: $('#examino').val(),
                esf: {
                    izq: $('#esfIzq').val(),
                    der: $('#esfDer').val()
                },
                cil: {
                    izq: $('#cilIzq').val(),
                    der: $('#cilDer').val()
                },
                eje: {
                    izq: $('#ejeIzq').val(),
                    der: $('#ejeDer').val()
                },
                add: {
                    izq: $('#addIzq').val(),
                    der: $('#addDer').val()
                },
                lentes: {
                    nota: $('#nota').val(),
                    lente: $('#lente').val(),
                    color: $('#color').val(),
                    armazon: $('#armazon').val(),
                    oblea: $('#oblea').val(),
                    di: $('#di').val(),
                    observaciones: $('#observaciones').val()
                }
            }
        };
        trimUp(params);
        trimUp(params.consulta);
        trimUp(params.consulta.lentes);
        const {
            nombre,
            paterno,
            materno
        } = params;
        if (nombre && paterno && materno) {
            let url = "client/add";
            asyncPostReq(params, url, 'application/json')
            .then( data =>{
                if (data) {
                    if (data.status === "F") {
                        createNotify('pe-7s-close-circle', `El usuario ya existe, <b>INTENTE de nuevo</b>.`, 'danger', 1000, 5000);
                        $('#nombre').val("");
                        $('#paterno').val("");
                        $('#materno').val("");
                        $('#edad').val("");
                    } else {
                        createNotify('pe-7s-check', `<b>EXITO</b>, Usuario creado correctamente.`, 'success', 1000, 5000);
                        $('#idFormNewUser').trigger("reset");
                    }
                }
            }).catch(e => console.error(e));
        } else {
            createNotify('pe-7s-attention', `<b>Complete</b> el formulario para continuar.`, 'warning', 1000, 5000);
        }
        $('#basics-tab').tab('show');
    }
});