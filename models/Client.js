var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LentesSchema = new Schema({
    nota : String,
    lente : String,
    color : String,
    armazon : String,
    oblea : String,
    di : String,
    observaciones : String
});

var ConsultaSchema = new Schema({
    fecha: { type: Date, required: true},
    examino : String,
    esf : { izq : String, der : String },
    cil : { izq: String, der: String },
    eje : { izq: String, der: String },
    add : { izq: String, der: String },
    lentes : [LentesSchema]
});

var ClientSchema = new Schema({
    nombre : { type : String, required : true, max : 40 },
    paterno : { type : String, required : true, max : 40 },
    materno : { type : String, required : true, max : 40 },
    edad : String,
    usa : String, //Usa lentes
    trabajo: String,
    telefono : String,
    direccion : String,
    poblacion : String,
    registro: Date,
    consultas : [ConsultaSchema],
    estado: {fecha: Date, usuario: String, tipo: String}
});

module.exports = mongoose.model('Client', ClientSchema);