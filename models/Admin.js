var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AdminSchema = new Schema({
    user: { type: String, required: true, max: 40 },
    pass: { type: String, required: true, max: 40 }
});

module.exports = mongoose.model('Admin', AdminSchema);