var Client = require("../models/Client");
//var xlsxParser = require('xlsx-parse-json');
/*var FileAPI = require('file-api'),
    File = FileAPI.File;*/
var ClientController = {};

ClientController.add = (req, res) => {
    const {nombre,paterno,materno,consulta} = req.body;
    Client.findOne({
        nombre: nombre,
        paterno: paterno,
        materno: materno
    }).exec((err, usuario)=>{
        if(!usuario){
            if(err){console.log('Error',err);return;}
            const hoy = new Date();
            consulta.fecha = (!consulta.fecha)?hoy:`${consulta.fecha} 00:00:00`;
            let newClient = new Client(req.body);
            newClient.registro = hoy;
            newClient.consultas = [...newClient.consultas, consulta];
            newClient.estado = {fecha: hoy, usuario: req.session.user, tipo: "Creado"};
            newClient.save((err) => {
                if(err){console.log('Error', err);return;}
                res.send({status:"S"});
            });
        }
        else
            res.send({status:"F"});
    });
};

ClientController.listAll = (req, res) => {
    const campos = '-consultas -estado -__v';
    Client.find({}, campos).collation({locale:'es',strength: 2}).sort({nombre:1}).exec((err, usuarios) => {
        if (err) { console.error(err); return; }
        res.send(usuarios);
    });
};

ClientController.getConsultById = (req, res) => {
    const idConsulta = req.body.idConsulta;
    const condition = {consultas : {$elemMatch : {_id : idConsulta}}}; //Seleccionar al usuario de la consulta correspondiente
    const seleccion = { //Filtrar los resultados
        "_id" : 0,
        consultas : {$elemMatch : {_id : idConsulta}},
        "consultas.esf": 1,
        "consultas.cil": 1,
        "consultas.eje": 1,
        "consultas.add": 1,
        "consultas._id": 1,
        "consultas.lentes.nota": 1
    };
    Client.findOne(condition, seleccion)
    .exec((err, consulta) => {
        if (err) { console.log(err); return;}
        console.log(consulta);
        res.send(consulta);
    });
};

ClientController.getNotaById = (req, res) => {
    const idNota = req.body.idNota;
    const condition = {consultas : {$elemMatch : {_id : idNota}}}; //Seleccionar al usuario de la consulta correspondiente
    const seleccion = { //Filtrar los resultados
        "_id" : 0,
        consultas : {$elemMatch : {_id : idNota}},
        "consultas.lentes": 1
    };
    Client.findOne(condition, seleccion)
    .exec((err, consulta) => {
        if (err) { console.log(err); return;}
        console.log(consulta);
        res.send(consulta.consultas[0].lentes[0]);
    });
};

ClientController.doSearch = (req, res) => {
    const parametros = req.body;
    let like = {};
    for (const key in parametros)
        like[key] = new RegExp(`${parametros[key]}`,'i');
    const campos = '-consultas';
    Client.find(like, campos).collation({locale:'es',strength: 2}).sort({nombre:1}).exec((err, usuarios) => {
        if (err) { console.error(err); return; }
        res.send(usuarios);
    });
};

ClientController.details = (req, res) => {
    const id = req.body.user;
    Client.findById(id, 'nombre paterno materno edad registro consultas._id consultas.examino consultas.fecha consultas.lentes.nota').exec((err, details) => {
        if (err) { console.error(err); return; }
        res.send(details);
    });
};

ClientController.oneById = (req, res) => {
    const id = req.body.id;
    Client.findById({_id:id}, '-consultas -registro -_id -__v').exec((err, dataUser) => {
        if (err) { console.error(err); return; }
        res.send(dataUser);
    });
};

ClientController.addTest = (req, res) => {
    const id = req.body.id;
    console.log(req.body);
    let consulta = req.body.consulta;
    const hoy = new Date();
    consulta.fecha = (!consulta.fecha) ? hoy : `${consulta.fecha} 00:00:00`;
    Client.findByIdAndUpdate({ _id: id }, { //encontrar uno y hacer un update pero ordenarlos al mismo tiempo
        $push: {
            "consultas": {
                $each: [consulta],
                $sort: { fecha: -1 }
            }
        }
    }).exec((err, usuario) => {
        if (err) { console.log('Error: ', err); return; }
        Client.findOneAndUpdate({ _id: id },
            { $set: { estado: { fecha: hoy, usuario: req.session.user, tipo: "Nueva Consulta" } } },
            (err, doc) => {
                if (err) { console.error(err); res.send({ status: "F" }); return; }
                res.send({ status: "S" });
            }
        );
    });
};

/*ClientController.load = (req, res) => {
    if (!req.files) {
        res.send({status:"F"});
        return;
    }
    else{
        const file = req.files.foo;
        file.mv((`./public/uploads/${file.name}`), (err) => {
            console.log(err)
        });
        xlsxParser.onFileSelection(new File(`./public/uploads/${file.name}`)).then((data) => {
            if(data)
                res.send(data);
            else
                res.send({status:"F"});
        });
    }
};*/

ClientController.editUser = (req, res) => {
    const hoy = new Date();
    let data = req.body;
    data.estado = {fecha: hoy, usuario: req.session.user, tipo: "Editado"};
    console.log(data);
    Client.findOneAndUpdate(
        {_id:req.body.id}, //identificador
        { $set: data }, //data a modificar
        //dentro de $set se pueden especificar el conjunto de datos a modificar
       // { new: true }, //retornar el documento modificado
        (err, doc) => { //funcion callback...
            if (err) { console.error(err); res.send({status:"F"}); return; }
            //console.log(doc);
            res.send({status:"S"});
        }
    );
};

ClientController.deleteUser = (req, res) => {
    Client.findByIdAndRemove(
        {_id:req.body.id}, //identificador
        (err, doc) => { //funcion callback...
            if (err) { console.error(err); res.send({status:"F"}); return; }
            //console.log(doc);
            res.send({status:"S"});
        }
    );
};

ClientController.updateTest = (req, res)=>{
    const idConsulta = req.body.id;
    const consulta = req.body.consulta;
    const condition = {consultas : {$elemMatch : {_id : idConsulta}}}; //Seleccionar al usuario de la consulta correspondiente
    const seleccion = { //Filtrar los resultados
        "_id" : 0,
        consultas : {$elemMatch : {_id : idConsulta}},
        "consultas.examino": 1,
        "consultas.fecha": 1
    };
    Client.findOne(condition, seleccion)
    .exec((err, result) => {
        if (err) { console.log(err); return;}
        if(result){
            const {examino, fecha} = result.consultas[0];
            consulta.examino = examino;
            consulta.fecha = fecha;
            console.log(consulta);
            Client.findOneAndUpdate(condition,
                { $set: { 'consultas.$': consulta} },
                (err, doc) => {
                    if (err) { console.error(err); res.send({ status: "F" }); return; }
                    res.send({ status: "S" });
                }
            );
        }
    });
};

ClientController.dropTest = (req, res)=>{
    const idConsulta = req.body.id;
    const condition = {consultas : {$elemMatch : {_id : idConsulta}}};
    Client.findOneAndUpdate(condition,
        { $pull: {consultas : {_id : idConsulta}} },
        (err, doc) => {
            if (err) { console.error(err); res.send({ status: "F" }); return; }
            if (doc) res.send({ status: "S" }); else res.send({ status: "F" });
        }
    );
};

module.exports = ClientController;