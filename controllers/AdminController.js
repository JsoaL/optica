var Admin = require("../models/Admin");
var AdminController = {};

AdminController.create = function (req, res){res.render('../views/admin/create');};
AdminController.save = function (req, res){
    console.log(req.body);
    var admin = new Admin(req.body);
    admin.save(function (err) {
        if (err) { console.log('Error: ', err); return; }
        console.log("Successfully created a user.");
        res.redirect("/admin/create");
    });
};
AdminController.login = function (req, res){
    Admin.findOne(req.body).exec((err, usuario)=>{
        if(usuario){
            if(err){console.log('Error',err);return;}
            req.session.user = req.body.user;
            res.send({status:"S"});
        }
        else
            res.send({status:"F"});
    });
};

module.exports = AdminController;