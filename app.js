//var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var session = require('express-session');
var fileUpload = require('express-fileupload');

//Routes imports
var indexRouter = require('./routes/index');
var adminRouter = require('./routes/admin');
var clientRouter = require('./routes/client');

//Base Connection
var mongoDB = 'mongodb://db_main:27017/Optica';

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

app.use(fileUpload());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret: 'optica', resave: true, saveUninitialized: true }));

//Routes
app.use('/', indexRouter);
app.use('/admin', adminRouter);
app.use('/client', clientRouter);

//Ruta para desloguear
app.get('/logout', (req, res) => {
  req.session.destroy((err) => {
    if (err) { res.send({status:"F"}); return;}
    res.send({status:"S"});
  });
});

//BDs
mongoose.connect(mongoDB, { useNewUrlParser: true,useFindAndModify: false });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// catch 404 and forward to error handler
app.use(function(req, res) {
  res.status(404).send('Not Found');
});

// error handler
app.use(function(err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send('error');
});

module.exports = app;
